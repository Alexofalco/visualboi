![Title](https://gitlab.com/Alexofalco/visualboi/-/raw/master/docs/title.png)

# VisualBOI
Visual BOI è un progetto nato con lo scopo di emulare su pc la famosa console a 8 bit GHEIMBOI. 

* Supporto completo alle rom convertite
* Audio mono riprodotto fedelmente 
* Grafica nativa 160x144 pixel con palette colori originale, zoom x2 abilitato
* Switch power on/off emulato via software!!! ora non dovrai più chiudere e riaprire il programma per riavviare il gioco

## Getting started
* Scaricare dal seguente link l'ultima versione del VisualBOI [Link download](https://gitlab.com/Alexofalco/visualboi/-/raw/master/dist/visualBOI.zip?inline=false)
* Estrae il file zippato in una cartella del vostro pc
* Eseguire il software VisualBOI.exe

## Roms

Per poter giocare è necessario inserire le rom desiderate nella cartella ./roms

Qui di seguito la lista delle rom supportate con la versione attuale del software:

* [ ] *Aereocombattimento*
* [ ] *Compactmon Pink and Violet version*
* [x] *Flappy bunny*  [Link download](https://gitlab.com/Alexofalco/visualboi/-/raw/7336fd76b360b3928b57b773dd47fc6b493c6a5e/dist/flappy_bunny.zip?inline=false)
* [ ] *Miniman: Dr Wally's Revenge*
* [ ] *Serious Jak* [Link download](https://gitlab.com/Alexofalco/visualboi/-/raw/master/dist/serious_jak.zip?inline=false)
* [ ] *Super Giovanni Brioche 2*
* [ ] *Z-Fight*

## Comandi di gioco
Durante l'emulazione è possibile giocare attraverso l'uso dei seguenti tasti
* Frecce direzionali: d-pad
* D: Pulsante A
* S: Pulsante B
* Enter: Start
* Backspace: Select
* K: Switch power on/off

## Problemi noti e/o bugs

VisualBOI è stato rilasciato in versione beta.
Per ogni problema riguardo l'emulazione e le rom rilasciate ti consigliamo di consultare il nostro [IssueTracking](https://gitlab.com/Alexofalco/visualboi/-/issues?scope=all&utf8=%E2%9C%93&state=all).

Grazie per il supporto! <3